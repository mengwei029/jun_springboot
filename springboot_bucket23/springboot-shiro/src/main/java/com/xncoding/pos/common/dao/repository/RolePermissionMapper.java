package com.xncoding.pos.common.dao.repository;

import com.xncoding.pos.common.dao.entity.RolePermission;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 角色权限关联表 Mapper
 *
 * @author Wujun
 * @version 1.0
 * @since 2018/01/02
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
