/*
 * Copyright 2018 kiwipeach.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.kiwipeach.demo.bean;

/**
 * 兴趣爱好
 *
 * @author Wujun
 * @create 2018/07/01
 */
public class Hobby {
    private String hobName;
    private String hobDesc;

    public String getHobName() {
        return hobName;
    }

    public void setHobName(String hobName) {
        this.hobName = hobName;
    }

    public String getHobDesc() {
        return hobDesc;
    }

    public void setHobDesc(String hobDesc) {
        this.hobDesc = hobDesc;
    }
}
