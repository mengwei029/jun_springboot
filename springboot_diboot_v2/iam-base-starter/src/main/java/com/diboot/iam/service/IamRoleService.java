package com.diboot.iam.service;

import com.diboot.iam.entity.IamRole;

/**
* 角色相关Service
* @author Wujun
* @version 2.0
* @date 2019-12-03
*/
public interface IamRoleService extends BaseIamService<IamRole> {

}