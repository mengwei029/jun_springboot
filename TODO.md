补充TODO待办清单
 
springboot     *****

  <parent>
    <groupId>com.jun.plugin</groupId>
	<artifactId>jun_springboot</artifactId>
	<version>1.0</version>
  </parent>
 com.jun.plugin
           
springboot_batch  调整
springboot_bboot    干掉
springboot_blogtemplatewithbootstrap   挪走到jun_frontend
springboot_bootshiro    调整
springboot_bootstrapcomment      挪走到jun_frontend
springboot_bootutil   调整  springboot_util 
springboot_bucket  调整
springboot_bucket23   干掉
springboot_cache    调整，新增
springboot_cache_ehcache   调整
springboot_cache_redis    调整
springboot_camel   干掉
springboot_cloud   放到springcloud里面
springboot_cloud 2   干掉
springboot_codegen   调整
springboot_codegenerator   调整
springboot_codegenerator555   干掉
springboot_common_boot_email   调整
springboot_cronboot   干掉，挪走到springboot_quartz
springboot_crud_demo   调整，去掉
springboot_data_jpa   调整
springboot_data_mybatis   调整
springboot_demo   不动
springboot_demo 4   挪走到demo里面
springboot_demo_starter   干掉
springboot_demo2  合并到demo里面
springboot_demo2 2   合并到demo里面
springboot_demo11   挪走到demo里面
springboot_demo21  干掉
springboot_demo22  干掉
springboot_demo23  干掉
springboot_demo223  干掉
springboot_diboot_v2   干掉
springboot_distributed_seckill       调整
springboot_docker   调整
springboot_drools_springboot     调整到springboot_drools
springboot_drools7_demo  合并到springboot_drools
springboot_dubbo  调整
springboot_dubbo_consumer   干掉
springboot_dubbo_consumer23  干掉
springboot_dubbo_provider  干掉
springboot_dubbo_provider23  干掉
springboot_dubbox  调整
springboot_dubbox_web   干掉
springboot_dynamic_datasource  调整
springboot_dynamic_datasource_spring_boot_starter   干掉、合并
springboot_dynamic_datasource_spring_boot_starter2  干掉、删掉
springboot_dynamicds_spring_boot   干掉、合并
springboot_elastic_job   调整下
springboot_elasticsearch  调整
springboot_elasticsearch 2  干掉、合并
springboot_elasticsearch 3  干掉、合并
springboot_elasticsearch_rest_high_level_client   干掉、合并
springboot_elasticsearch2   干掉、合并
springboot_elasticsearch3  干掉
springboot_email  调整
springboot_eshelloword_booter   干掉
springboot_eshelloword_spring_boot_starter  干掉
springboot_example_ui     合并到springboot_demo
springboot_examples  合并到springboot_demo
springboot_examples22    合并到springboot_demo
springboot_exception_handler  调整
springboot_fastdfs       调整
springboot_fastdfs 2   干掉
springboot_fastdfs21  干掉
springboot_filemanager   调整
springboot_filemanager2   干掉
springboot_filemanager13  干掉
springboot_filemanager23  干掉
springboot_flyway   调整
springboot_freemarker   调整
springboot_graylog       调整
springboot_helloworld  调整
springboot_helloworld 2   干掉
springboot_helloworld 3  干掉
springboot_https   调整
springboot_integration   迁移到springboot_demo里面
springboot_jackson_datetime_example   调整 jackson
springboot_java_springboot_sell  调整
springboot_jboot   干掉
springboot_jbpm6   调整
springboot_jediscluster   调整
springboot_jfinal_bootstrap_table     调整
springboot_jms   调整

 springboot_jms   调整


https://juejin.im/post/6844903826927910926
springboot_jpa   干掉
springboot_jpa2  调整
springboot_jshow_for_bootstrap   挪走jun_frontend
springboot_junit  调整

https://github.com/anselleeyy/logistics-front
https://github.com/anselleeyy/logistics-back

来自 <https://github.com/anselleeyy/logistics-front> 

springboot_jwt  調整
springboot_jwt 2  干掉
springboot_jwt2  合并
springboot_kafka_demo   合并到springboot_mq_kafka
springboot_kaptcha_spring_boot_starter   调整
springboot_kisso_spring_boot  调整
springboot_klock_starter  调整
springboot_klock_starter22   干掉
springboot_labs 调整
springboot_laydate_theme_bootstrap   迁移jun_frontend
springboot_ldap   调整
springboot_learning   迁移到springboot_demo
springboot_learning_example  迁移到springboot_demo
springboot_learning23   迁移到springboot_demo
springboot_leave   干掉
springboot_lhbauth   调整
springboot_lock_spring_boot_starter   合并
springboot_lock4j_spring_boot_starter   合并
springboot_log   调整，合并
springboot_log 2   干掉
springboot_log_aop   调整
springboot_logback   调整，合并
springboot_mall_boot   调整
springboot_maven   干掉
springboot_mongodb   调整
springboot_mongodb 2   合并
springboot_mongodb2  干掉
springboot_mongodb23   合并
springboot_mq_kafka   调整
springboot_mq_rabbitmq   调整
springboot_mq_rocketmq   调整
springboot_ms_office       调整
springboot_multi_datasource_jpa   调整
springboot_multi_datasource_mybatis   调整
springboot_mybatis   调整
springboot_mybatis_jsp   合并
springboot_neo4j   调整
springboot_oauth   调整
springboot_oauth_server    调整
springboot_oauth2_server   调整
springboot_order_mybaits_activemq   调整
springboot_orm_beetlsql   调整
springboot_orm_jdbctemplate   调整
springboot_orm_jpa     调整
springboot_orm_mybatis   调整
springboot_orm_mybatis_mapper_page   调整
springboot_orm_mybatis_plus   调整
springboot_pay   调整
springboot_pay 2   调整，合并
springboot_pay222       干掉
springboot_plus   迁移到jun_framework
springboot_project     迁移到jun_framework
springboot_properties   调整
springboot_ratelimit_guava    调整
springboot_ratelimit_redis   调整
springboot_rbac_security   调整
springboot_rbac_shiro   调整
springboot_redis   合并
springboot_redis_sentinel_example   调整
springboot_redislock     调整
springboot_redisson_spring_boot_starter   调整
springboot_restful   合并
springboot_restful 2   调整
springboot_roncoo_jui_springboot   挪走jun_framework
springboot_roncoo_spring_boot   调整
springboot_samples   合并到springboot_demo
springboot_session    调整
springboot_session2  合并
springboot_session3  合并
springboot_sharding_jdbc   调整
springboot_sharding_jdbc_demo   合并
springboot_sharding_jdbc_mybatis_plus_spring_boot_starter   合并
springboot_shiro   调整
springboot_shiro_spring_boot_starter   合并
springboot_smart_boot  迁移到jun_framework
springboot_social   调整
springboot_sofa_boot   迁移到jun_framework
springboot_springside4   调整
springboot_ssh2_boot1   调整
springboot_starter_drools      调整
springboot_starter_dubbo  调整
springboot_starter_dubbox  调整
springboot_starter_swagger   调整
springboot_starter_weixin   挪走
springboot_startkit  挪走到frame
springboot_study  合并到demo 中
springboot_study 2 合并到demo
springboot_superboot   迁移到springcloud里面
springboot_swagger  调整
springboot_swagger_beauty   调整
springboot_task  调整
springboot_task_quartz   调整
springboot_task_xxl_job  调整
springboot_task2   干掉
springboot_template_beetl 
springboot_template_enjoy 
springboot_template_freemarker 
springboot_template_thymeleaf 
springboot_templatemaven   迁移到meven_template
springboot_templates_layui_admin   调整
springboot_test_examples     迁移到demo里面
springboot_tio   干掉
springboot_tools   干掉
springboot_transaction   迁移到demo
springboot_tyboot   迁移到frame
springboot_tyboot2   干掉
springboot_tybootdemo   迁移到frame
springboot_uflo  干掉
springboot_uniapp 
springboot_unity      调整
springboot_unity 2   干掉
springboot_unity23  干掉
springboot_unity23 2   干掉
springboot_upload    调整
springboot_ureport2   干掉
springboot_urule   干掉
springboot_war   调整 
springboot_webflux_reactive_rest_api   调整
springboot_webhook   调整
springboot_webhook 2   干掉
springboot_websocket   调整
springboot_websocket_demo   干掉、合并
springboot_websocket_socketio   调整
springboot_websocket2   合并
springboot_weixin_dubbo_springboot    迁移到weixin
springboot_wm_accesstoken   迁移到微信

springboot_zookeeper   调整  

